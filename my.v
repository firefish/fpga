module top(						//top module
	CLK,
	BUT1,
	BUT2,
	LED1,
	LED2,
	TXD,
	MIC1_CLK,
	MIC1_DAT,
	MIC1_VCC,
	MIC1_GND,
	MIC2_CLK,
	MIC2_DAT,
	MIC2_VCC,
	MIC2_GND,
	MIC3_CLK,
	MIC3_DAT,
	MIC3_VCC,
	MIC3_GND,
	MIC4_CLK,
	MIC4_DAT,
	MIC4_VCC,
	MIC4_GND
);

input			CLK;				//input 100Mhz clock
input			BUT1;				//input signal from button 1
input			BUT2;				//input signal from button 2
output			LED1;				//output signal to LED1
output			LED2;				//output signal to LED2
output			TXD;
output			MIC1_CLK;
input			MIC1_DAT;
output			MIC1_VCC;
output			MIC1_GND;
output			MIC2_CLK;
input			MIC2_DAT;
output			MIC2_VCC;
output			MIC2_GND;
output			MIC3_CLK;
input			MIC3_DAT;
output			MIC3_VCC;
output			MIC3_GND;
output			MIC4_CLK;
input			MIC4_DAT;
output			MIC4_VCC;
output			MIC4_GND;

parameter integer BAUD_RATE = 921_600;
parameter integer CLOCK_FREQ_HZ = 100_000_000;
localparam integer PERIOD = CLOCK_FREQ_HZ / BAUD_RATE;

rs232_send #(
	.PERIOD(PERIOD)
) send (
	.clk  (CLK ),
	.TX   (TXD  ),
//	.data (8'b00110000)
	.data (cntr[7:0])
);

reg			BUT1_r;
reg			BUT2_r;
reg		[14:0]	cntr;
reg		[12:0]	clk_div;

wire			clk_12KHz;
wire			clk_3MHz;

assign clk_12KHz = clk_div[12];				//100 000 000 Hz / 8192 = 12207 Hz
assign clk_3MHz = clk_div[4];

assign MIC1_CLK = clk_3MHz;
assign MIC2_CLK = clk_3MHz;
assign MIC3_CLK = clk_3MHz;
assign MIC4_CLK = clk_3MHz;

assign MIC1_GND = 0;
assign MIC2_GND = 0;
assign MIC3_GND = 0;
assign MIC4_GND = 0;

assign MIC1_VCC = 1;
assign MIC2_VCC = 1;
assign MIC3_VCC = 1;
assign MIC4_VCC = 1;

always @ (posedge CLK) begin				//on each positive edge of 100Mhz clock increment clk_div
	clk_div <= clk_div + 12'b1;
end

always @ (posedge clk_3MHz) begin
	BUT1_r <= BUT1;					//capture button 1 state to BUT1_r
	BUT2_r <= BUT2;					//capture button 2 state to BUT2_r
    if (MIC1_DAT == 1) begin
        cntr <= cntr + 15'd1;
    end
    else begin
        cntr <= cntr - 15'd1;
    end
end

endmodule

module rs232_send #(
	parameter integer PERIOD = 10
) (
	input  clk,
	output TX,
	input  [7:0] data
);
	reg [$clog2(PERIOD):0] cycle_cnt = 0;
	reg [5:0] bit_cnt = 0;

	always @(posedge clk) begin
		cycle_cnt <= cycle_cnt + 1;
		if (cycle_cnt == PERIOD-1) begin
			cycle_cnt <= 0;
			bit_cnt <= bit_cnt + 1;
			if (bit_cnt == 15) begin
				bit_cnt <= 0;
				data_byte <= data;
			end
		end
	end

	reg [7:0] data_byte;
	reg data_bit;

	always @(posedge clk) begin
		data_bit = 'bx;
		case (bit_cnt)
			0: data_bit <= 0; // start bit
			1: data_bit <= data_byte[0];
			2: data_bit <= data_byte[1];
			3: data_bit <= data_byte[2];
			4: data_bit <= data_byte[3];
			5: data_bit <= data_byte[4];
			6: data_bit <= data_byte[5];
			7: data_bit <= data_byte[6];
			8: data_bit <= data_byte[7];
			9: data_bit <= 1;  // stop bit
			10: data_bit <= 1; // stop bit
			default: data_bit <= 1; // idle
		endcase
	end

	assign TX = data_bit;
endmodule